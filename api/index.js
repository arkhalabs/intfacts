require("dotenv").config();

const express = require("express");
const router = express.Router();
const app = express();

const routingbot = require("./src/bot/routingbot.js");

router.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request);
  Object.setPrototypeOf(res, app.response);
  req.res = res;
  res.req = req;
  next();
});

router.post("/telegram", (req, res) => {
  if (
    "message" in req.body &&
    "entities" in req.body.message &&
    req.body.message.entities[0].type === "bot_command"
  ) {
    //si le message reçu est une commande
    let command = req.body.message.text.split(" ");
    let uid = req.body.message.from.id;
    let cid = req.body.message.chat.id;

    routingbot(command, cid, uid).then(data => {
      res.set("Content-Type", "application/json").send(data); //data est un array qui contient tout ce qui va bien pour la réponse à TG
    });
  } else {
    res.status(200).send("commande inconnue");
  }
});

router.post("/auth", (req, res) => {
  console.log(req.body);
  res.status(200).send("bien reçu");
});

router.get("/telegram", (req, res) => {
  console.log(req);
  res.send("okget");
});

router.post("/intfacts", (req, res) => {
  //firebase.addentry(req.data, 'intfacts').then((data) => res.send(data))
});

module.exports = {
  path: "/api",
  handler: router
};

/*
export default function(req, res, next) {
  // req is the Node.js http request object
  console.log(req.method)
  console.log(req.body)
  console.log(req.url)

  console.log(Object.getOwnPropertyNames(res))
  // res is the Node.js http response object

  // next is a function to call to invoke the next middleware
  // Don't forget to call next at the end if your middleware is not an endpoint!
  next()
}
*/
/*
export default {
  path: '/api/telegram',
  handler(req, res) {
    //apitg.post(req, res)
    console.log(req.body)
    console.log(req.method)
    res.end(`{test: 'Everything ok!'}`)
  }
}*/
