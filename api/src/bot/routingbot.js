const func = require("./routing.js");

module.exports = async (command, cid, uid) => {
  let json = {
    method: "sendMessage",
    chat_id: cid,
    text: "",
    parse_mode: "Markdown",
    disable_web_page_preview: true
  };

  command = func.cleantags(command);

  switch (command[0].toLowerCase()) {
    case "/article": //on balance une entrée au hasard
    case ":article":
    case "/article@intfacts_tg_bot":
      json.text = await func.articles(cid, command);
      return json;
    case "/siteref": //référence précise à un article "officiel"
    case "/siteref@intfacts_tg_bot":
      json.text = await func.siteref(cid, command, "ref");
      return json;
    case "/bimensuel":
    case "/bimensuels":
    case "/biweekly":
    case "/bimensuel@intfacts_tg_bot":
    case "/biweekly@intfacts_tg_bot":
      json.text = await func.siteref(cid, command, "bimensuel");
      return json;
    case "setcountdown":
      break;
    case "/intfact":
    case "/intfact@intfacts_tg_bot":
      json.text = await func.intfacts(cid, command);
      return json;
    case "/help":
    case "/commands":
    case "/help@intfacts_tg_bot":
    case "/commands@intfacts_tg_bot":
      json.text = await func.help(cid);
      return json;
    case "/price":
    case "/price@intfacts_tg_bot":
      json.text = await func.price(cid, command);
      return json;
    case "/mybag":
    case "/mybag@intfacts_tg_bot":
      json.text = await func.mybag(cid, command);
      return json;
    /*case '/leave':
      json.text = await func.leave(cid, uid, command)
      return json*/
    default:
      json.text = "Commande inconnue.";
      return "commande inconnue !";
  }
};
