//Récupère le JSON d'un lien firebase
//corrige les tags si besoin
//transforme en array potable
//ajoute la key dans chaque objet

module.exports = (data) => {
  let keys = Object.keys(data)
  let array = []

  keys.forEach((key) => {
    data[key].key = key
    if (typeof data[key].tags === 'string')
      data[key].tags = data[key].tags.split(',')
    array.push(data[key])
  })

  return array
}
