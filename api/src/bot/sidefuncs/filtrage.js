const accents = require('remove-accents')

/**
 *
 * @param {array} data  - Un tableau d'articles
 * @param {array} tags  - Un tableau de tags
 * @returns {array}     - Un tableau d'articles filtrés par tags
 */
module.exports = (data, tags) => {
  //data contient les entrées à trier, et tags les tags demandés
  let tridata = data.filter((el) => {
    let success = true
    if (tags.length > 0) {
      tags.forEach((tag) => {
        if (!el.tags.includes(accents.remove(tag.toLowerCase()))) {
          success = false
        } else {
        }
      })
    }

    return success
  })

  return tridata
}
