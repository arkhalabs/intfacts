const accents = require('remove-accents')

/* Cette fonction prend un array de tags et retourne un array de tags
 sans accents, sans majuscules*/

module.exports = (tags) => {
  let array = []
  accents
    .remove(tags.toString().toLowerCase())
    .split(',')
    .forEach((tag) => {
      array.push(tag.trim())
    })
  return array
}
