const axios = require('axios')
const jsontoarray = require('../sidefuncs/jsontoarray.js')
//const filtrage = require('../sidefuncs/filtrage.js')
const randomint = require('../sidefuncs/randomint.js')

/**
 *
 * @param {string} cid    - l'ID du chat Telegram
 * @param {array} command - un array contenant la commande, splittée en ' '
 * @returns {Promise<*>}  - un article choisi au hasard
 */
module.exports = async (cid, command) => {
  let tags = command.slice(1)
  if (tags.length > 0 && tags[0] === 'help') {
    return `La commande \`/intfact\` sélectionne un recoupement de sources associé à une interprétation, au hasard. Chaque entrée est associée à un flag indiquant, sur une échelle de 1 à 5, le niveau de certitude. 
        `
  } else {
    let data = await axios.get(`${process.env.DB_URL}/intfacts.json`)
    data = data.data

    data = jsontoarray(data)

    let resultat = data //uniquement pour prévoir un éventuel tri, c'est ici qu'on filtre
    let randomindex = randomint(0, resultat.length - 1)
    let chosenone = resultat[randomindex]

    delete chosenone.key
    let sources = '\n'
    if (typeof chosenone.sources === 'object' && chosenone.sources.length > 0) {
      chosenone.sources.forEach((item) => {
        sources += `[${item.description}](${item.url})\n`
      })
    } else {
      sources += `Aucune`
    }

    return `
*INTFACT*
*auteur* : ${chosenone.auteur}   *flag* : ${chosenone.flag}
*date* : ${chosenone.date}
*digest* : ${chosenone.titre}
*contenu* : ${chosenone.resume}
*Sources* : ${sources}`
  }
}
