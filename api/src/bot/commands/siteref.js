const axios = require('axios')
const jsontoarray = require('../sidefuncs/jsontoarray.js')
const filtrage = require('../sidefuncs/filtrage.js')
const randomint = require('../sidefuncs/randomint.js')
const tagtest = require('../sidefuncs/msgtags.js')

/**
 *
 * @param {string} cid    - l'ID du chat Telegram
 * @param {array} command - un array contenant la commande, splittée en ' '
 * @param {string} kind - la nature de la commande
 * @returns {Promise<*>}  - un article choisi au hasard
 */
module.exports = async (cid, command, kind) => {
  let tags = command.slice(1)
  if (tags.length > 0) tags.push(kind)
  else tags = [kind]
  let data = await axios.get(`${process.env.DB_URL}/articles.json`)
  data = data.data

  data = jsontoarray(data)
  if (tags.length > 1 && tags[0] === 'help') {
    let alltags = []
    data.forEach((entry) => {
      entry.tags.forEach((currtag) => {
        if (!alltags.includes(currtag)) alltags.push(currtag)
      })
    })
    return `
        *INTFACTS BOT*
        La commande \`/siteref\` permet d'obtenir la liste des sites de référence relatifs au projet INT Chain, en fonction des tags fournis en option.
        *Options :*
        \`/siteref [tags]\` permet de cibler les liens fournis autour des tags donnés. Vous pouvez renseigner autant de tags que vous voulez.
        Actuellement, ${alltags.length} tags disponibles.
        */!\\ N'inclue pas les bimensuels*
        Pour chercher dans les bimensuels, il faut utiliser \`/bimensuel\` ou \`/biweekly\`. L'utilisation est la même que pour \`/siteref\`.
        `
  } else {
    let resultat = filtrage(data, tags)

    let res_list = '\n'
    resultat.forEach((entry) => {
      res_list += `[${entry.description}](${entry.url})\n`
    })

    if (res_list !== '\n') {
      let msgtags = tagtest(command.slice(1))
      return `
Voici le(s) lien(s) correspondant à vos tags :
${res_list}
${msgtags}`
    } else {
      return `
Aucun site ne correspond à votre recherche.
${msgtags}`
    }
  }
}
