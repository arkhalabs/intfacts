const axios = require('axios')
const jsontoarray = require('../sidefuncs/jsontoarray.js')
const filtrage = require('../sidefuncs/filtrage.js')
const randomint = require('../sidefuncs/randomint.js')
const tagtest = require('../sidefuncs/msgtags.js')

/**
 *
 * @param {string} cid    - l'ID du chat Telegram
 * @param {array} command - un array contenant la commande, splittée en ' '
 * @returns {Promise<*>}  - un article choisi au hasard
 */
module.exports = async (cid, command) => {
  let tags = command.slice(1)

  if (tags.length > 0 && tags[0] === 'help') {
    return `
        La commande \`/article\` permet d'afficher un article concernant le projet INT Chain de près ou de loin, au hasard.
        *Options :*
        \`/article [tags]\` permet de cibler l'article autour d'une thématique définie. Vous pouvez renseigner autant de tags que vous voulez.
        Actuellement, tags disponibles.
        `
  } else {
    let data = await axios.get(`${process.env.DB_URL}/articles.json`)
    data = data.data

    data = jsontoarray(data)

    let resultat = filtrage(data, tags)
    let randomindex = randomint(0, resultat.length - 1)

    delete resultat[randomindex].key

    if (resultat.length > 0) {
      let chosenone = resultat[randomint(0, resultat.length - 1)]
      let msgtags = tagtest(command.slice(1))
      return `
Voici un article choisi au hasard :
[${chosenone.description}](${chosenone.url})
${msgtags}
Nombre d'articles correspondant : ${resultat.length}`
    } else {
      return `
Aucun article ne contient les tags ${commands.slice(1)}.`
    }
  }
}
