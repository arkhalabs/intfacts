const axios = require('axios')
const CoinGecko = require('coingecko-api')
const CGClient = new CoinGecko()

const arrondi = (value, decimals) => {
  return Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals)
}

module.exports.mybag = async (cid, command) => {
  let addr = command[1]
  let data = await axios.get(
    `https://explorer.intchain.io/api/wallet/walletDtail/?address=${addr}`
  )

  data = data.data
  data = data.data
  if (typeof data.balance !== 'undefined') {
    let sum = data.balance + data.stake
    let value = await CGClient.coins.fetch('internet-node-token', {})
    let price = value.data.market_data.current_price.usd
    let total = price * sum
    price = arrondi(price, 4)
    total = arrondi(total, 2)
    sum = arrondi(sum, 2)
    let msg = `Cours actuel : ${price} $
Total : ${sum} INT
Valeur de votre bag : ${total} $`
    return msg
  } else {
    let msg = `*/mybag* : L'adresse fournie n'est pas exacte (ou le serveur n'a pas envie de répondre). Vérifiez votre adresse, ou attendez un peu !`
    return msg
  }
}

module.exports.price = async (cid, command) => {
  let result = await CGClient.coins.fetch('internet-node-token', {})
  return `INT : ${result.data.market_data.current_price.usd}$`
}
