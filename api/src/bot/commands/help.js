module.exports = (cid) => {
  const msg = `
Commandes :\n
\`/help\` : Obtenir la liste des commandes disponibles.
\`/article [tags]\` : Sert un article/une vidéo en lien avec le projet INT Chain
    Exemple : \`/article iot\` vous donnera un article en lien avec l'IoT. \`/article help\` pour la liste des tags
\`/siteref [tags]\` : Sert le lien vers un article précis de la librairie officielle
    Exemples : \`/siteref wp\` , \`/siteref ama\` , etc... \`/siteref help\` pour la liste des liens
\`/bimensuel\` : affiche la liste des bimensuels répertoriés  
    Exemple: \`/bimensuel khipu\` affichera la liste des bimensuels relatifs à khipu.
\`/intfact\` : Sert un _intfact_ (un recoupement de sources interprétées, avec un degré de certitudes tout relatif)
\`/price\` : Affiche le prix actuel du INT
\`/mybag ADRESSE\` : Affiche le prix actuel du INT, le nombre de INT que vous possédez au total, ainsi que la valorisation actuelle de votre bag`

  return msg
}
