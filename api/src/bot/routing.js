module.exports.help = require('./commands/help.js')
module.exports.articles = require('./commands/articles.js')
module.exports.siteref = require('./commands/siteref.js')
module.exports.intfacts = require('./commands/intfacts.js')
module.exports.price = require('./commands/price.js').price
module.exports.mybag = require('./commands/price.js').mybag
module.exports.leave = require('./commands/leave.js')

module.exports.filtrage = require('./sidefuncs/filtrage.js')
module.exports.cleantags = require('./sidefuncs/cleantags.js')
