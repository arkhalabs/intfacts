import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

let app = null;

const firebaseConfig = {
  apiKey: process.env.APIKEY,
  authDomain: "intfacts-7b278.firebaseapp.com",
  databaseURL: "https://intfacts-7b278.firebaseio.com",
  projectId: "intfacts-7b278",
  storageBucket: "intfacts-7b278.appspot.com",
  messagingSenderId: "557818222078",
  appId: "1:557818222078:web:e4dd3487a7fb132e4ddcaf",
  measurementId: "G-8LT3DT0EET"
};

app = firebase.apps.length
  ? firebase.app()
  : firebase.initializeApp(firebaseConfig);

export const db = app.database();
