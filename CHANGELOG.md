# Changelog

Toutes les modifications notables apportées à ce projet seront consignées dans ce fichier.

Ce projet suit les recommandations de la [gestion sématique de version](https://semver.org/lang/fr/).

## Unreleased

- La commande `/mybag` enregistre un couple `[id_telegram, adresse INT]` pour faciliter la manipulation pour les fois suivantes
- Une commande `/add` permettant d'ajouter un article directement depuis un chan Telegram
- Une commande `/articles` permettant d'afficher la liste des articles correspondant aux tags fournis par l'utilisateur
- Console d'administration : envisager la possibilité d'un ajout/suppression d'un grand nombre de données.
- Intégration des jokes et de la FAQ dans le bot Telegram.

## [0.9.2] - 16/12/2019

Intégration du framework Nuxt.JS

### Changed

- Reprise complète du code pour la mise en place de l'application via Nuxt

### Added

- Jokes, FAQ sont désormais disponibles sur l'interface web
- Authentification pour la console d'administration

## [0.9.1] - 26/11/2019

Première version destinée au public.

### Changed

- Modification des commandes pour une meilleure lisibilité.
- Précisions des infos de la commande `/help`.
- Affichage des tags conditionné dans la réponse selon la présence ou non de tags dans la commande.
- Le bot répond désormais à toutes les commandes prévues suivies de `@intfacts_tg_bot`

### Added

- Une commande /leave pour que le bot quitte un chan.
- Console d'administration sommaire pour la gestion de la base de données
