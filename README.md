# intfacts-bot

Un bot pour accompagner le projet INT Chain sur Telegram.

## Commandes
- `/helpint` affiche l'aide associée aux commandes du bot.
- `/article [tags]` : Sert un article/une vidéo en lien avec le projet INT Chain.  
    Exemple : `/article iot` vous donnera un article en lien avec l'IoT. `/article help` pour la liste des tags
- `/siteref [tags]` : Sert le lien vers un article précis de la librairie officielle (*exclue les bimensuels*)  
    Exemples : `/siteref wp` , `/siteref ama` , etc... `/siteref help` pour la liste des liens
- `/bimensuel [tags]` : affiche la liste des bimensuels répertoriés  
Exemple: `/bimensuel khipu` affichera la liste des bimensuels relatifs à khipu.
- `/intfact` : Sert un _intfact_ au hasard (un recoupement de sources interprétées, avec un degré de certitudes tout relatif)
- `/price` : Affiche le prix actuel du INT
- `/mybag ADRESSE` : Affiche le prix actuel du INT, le nombre de INT que vous possédez au total, ainsi que la valorisation actuelle de votre bag`